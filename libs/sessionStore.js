var session = require('express-session');
var MongoStore = require('connect-mongo')(session),
    sessionStore = new MongoStore({ url: 'mongodb://localhost:27017/loginapp' });

module.exports = sessionStore;